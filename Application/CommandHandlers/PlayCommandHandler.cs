﻿using Cleverbit.CodingTask.Data;
using Cleverbit.CodingTask.Data.Models;
using Cleverbit.CodingTask.Data.Models.Commands;
using Cleverbit.CodingTask.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Application.CommandHandlers
{
    public class PlayCommandHandler : ICommandHandler<PlayCommand>
    {
        private CodingTaskContext _db;
        private IConfiguration _configuration;
        private static object _lock = new object();

        public PlayCommandHandler(CodingTaskContext dbContext, IConfiguration configuration)
        {
            _db = dbContext;
            _configuration = configuration;
        }

        public async Task<CommandResult> HandleAsync(PlayCommand command)
        {
            var expireTime = int.Parse(_configuration["ExpireTime"]);

            var currentMatch = await _db.Set<Match>().Include(x => x.Players).FirstOrDefaultAsync(x => x.StartDate >= DateTime.Now && x.StartDate.AddSeconds(expireTime) <= DateTime.Now);

            if (currentMatch == null)
            {
                currentMatch = Match.Create(DateTime.Now, expireTime);

                _db.Add(currentMatch);
            }

            var matchPlayers = currentMatch.Players.Select(x => x.UserId).ToList();

            if (matchPlayers.Contains(command.UserId))
            {
                return CommandResult.Failed("Player's Already Played this Match");
            }
            
            var number = new Random().Next(0, 100);

            var player = MatchPlayer.Create(command.UserId, number);

            currentMatch.AddPlayer(player);

            return CommandResult.Succeded(number);
        }
    }
}
