﻿using Cleverbit.CodingTask.Utilities;
using System;
using System.Collections.Generic;

namespace Application.Queries
{
    public class GetAllMatchQuery : IQuery
    {
    }

    public class GetAllMatchQueryResult : IQueryResult
    {
        public List<MatchItem> Matchs { get; set; }
    }

    public class MatchItem
    {
        public DateTime StartDate { get; set; }

        public List<MatchPlayerItem> Players { get; set; }
    }

    public class MatchPlayerItem
    {
        public string UserName { get; set; }

        public int Number { get; set; }

        public bool IsWinner { get; set; }
    }
}
