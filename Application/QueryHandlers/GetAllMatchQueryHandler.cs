﻿using Application.Queries;
using Cleverbit.CodingTask.Data;
using Cleverbit.CodingTask.Data.Models;
using Cleverbit.CodingTask.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Application.QueryHandlers
{
    public class GetAllMatchQueryExecutor : IQueryExecutor<GetAllMatchQuery, GetAllMatchQueryResult>
    {
        private CodingTaskContext _db;
        private IConfiguration _configuration;
        private int _expireTime;

        public GetAllMatchQueryExecutor(CodingTaskContext db, IConfiguration configuration)
        {
            _db = db;
            _configuration = configuration;
            _expireTime = int.Parse(_configuration["ExpireTime"]);
        }

        public async Task<GetAllMatchQueryResult> ExecuteAsync(GetAllMatchQuery query)
        {
            var matches = await _db.Set<Match>()
                                   .Include(x => x.Players)
                                   .ThenInclude(x=>x.User)
                                   .Where(x => DateTime.Now > x.StartDate.AddSeconds(_expireTime))
                                   .ToListAsync();

            var result = new GetAllMatchQueryResult
            {
                Matchs = matches.Select(x => new MatchItem
                {
                    StartDate = x.StartDate,
                    Players = x.Players.Select(s => new MatchPlayerItem
                    {
                        IsWinner = s.IsWinner,
                        Number = s.Number,
                        UserName = s.User.UserName
                    }).ToList()
                }).ToList()
            };

            return result;
        }
    }
}
