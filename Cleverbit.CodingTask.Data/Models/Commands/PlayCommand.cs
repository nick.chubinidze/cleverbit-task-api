﻿using Cleverbit.CodingTask.Utilities;

namespace Cleverbit.CodingTask.Data.Models.Commands
{
    public class PlayCommand : ICommand
    {
        public int UserId { get; set; }
    }
}
