﻿using Cleverbit.CodingTask.Data.Models.Shared;
using System;
using System.Collections.Generic;

namespace Cleverbit.CodingTask.Data.Models
{
    public class Match : BaseEntity
    {
        public DateTime StartDate { get; set; }

        public int ExpireTime { get; set; }

        public ICollection<MatchPlayer> Players { get; set; } = new HashSet<MatchPlayer>();

        public void AddPlayer(MatchPlayer player) => this.Players.Add(player);

        public static Match Create(DateTime startDate, int expireTime) => new Match { StartDate = startDate, ExpireTime = expireTime };
    }

    public class MatchPlayer
    {
        public int Id { get; set; }

        public User User { get; set; }
        public int UserId { get; set; }
        public Match Match { get; set; }
        public int MatchId { get; set; }
        public int Number { get; set; }
        public bool IsWinner { get; set; }

        public DateTime PlayDate { get; set; }

        public static MatchPlayer Create(int userId, int number)
        {
            return new MatchPlayer
            {
                UserId = userId,
                Number = number
            };
        }
    }
}
