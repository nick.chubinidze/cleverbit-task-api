﻿using System;

namespace Cleverbit.CodingTask.Data.Models.Shared
{
    public class BaseEntity
    {
        public int Id { get; set; }

        public bool IsDeleted { get; set; }

        public void Delete() => IsDeleted = true;

        public DateTime CreateDate { get; set; }
    }
}
