﻿using Application.Queries;
using Cleverbit.CodingTask.Data;
using Cleverbit.CodingTask.Data.Models.Commands;
using Cleverbit.CodingTask.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Cleverbit.CodingTask.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MatchController : BaseController
    {
        public MatchController(CommandExecutor commandExecutor, QueryDispatcher queryDispatcher, CodingTaskContext db) : base(commandExecutor, queryDispatcher, db)
        {
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetAsync()
        {
            return Ok(await QueryDispatcheAsync<GetAllMatchQuery, GetAllMatchQueryResult>(new GetAllMatchQuery()));
        }

        [HttpPost("play")]
        public async Task<IActionResult> PlayAsync(PlayCommand command)
        {
            command.UserId = int.Parse(User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value);

            var commandResult = await ExecuteCommandAsync(command);
            if (commandResult.Success)
                return Ok(commandResult);

            return BadRequest(commandResult);
        }
    }
}
