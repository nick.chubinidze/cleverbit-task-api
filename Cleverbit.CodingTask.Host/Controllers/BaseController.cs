﻿using Cleverbit.CodingTask.Data;
using Cleverbit.CodingTask.Utilities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Cleverbit.CodingTask.Host.Controllers
{
    public class BaseController : ControllerBase
    {
        private CommandExecutor _commandExecutor;
        private QueryDispatcher _queryDispatcher;
        private CodingTaskContext _db;

        public BaseController(CommandExecutor commandExecutor, QueryDispatcher queryDispatcher, CodingTaskContext db)
        {
            _commandExecutor = commandExecutor;
            _queryDispatcher = queryDispatcher;
            _db = db;
        }

        public async Task<CommandResult> ExecuteCommandAsync<TCommand>(TCommand command) where TCommand : ICommand
        {
            var result = default(CommandResult);
            try
            {
                result = await _commandExecutor.ExecuteAsync(command);
                if (result.Success)
                {
                    await SaveAsync();
                }

                return result;
            }
            catch (Exception)
            {

                return result;
            }
        }

        public async Task<TQueryResult> QueryDispatcheAsync<TQuery, TQueryResult>(TQuery query)
            where TQuery : IQuery where TQueryResult : IQueryResult
        {
            return await this._queryDispatcher.DispatcheAsync<TQuery, TQueryResult>(query);
        }

        private async Task SaveAsync()
        {
            if (this._db.ChangeTracker.HasChanges())
            {
                await _db.SaveChangesAsync();
            }
        }
    }
}
