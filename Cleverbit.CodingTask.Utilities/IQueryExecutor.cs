﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;


namespace Cleverbit.CodingTask.Utilities
{
    public interface IQueryExecutor<TQuery, TResult> where TQuery : IQuery where TResult : IQueryResult
    {
        Task<TResult> ExecuteAsync(TQuery query);
    }

    public class QueryDispatcher
    {
        private IServiceProvider _serviceProvider;

        public QueryDispatcher(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task<TQueryResult> DispatcheAsync<TQuery, TQueryResult>(TQuery query) where TQuery : IQuery where TQueryResult : IQueryResult
        {
            var queryExecutor = _serviceProvider.GetService<IQueryExecutor<TQuery, TQueryResult>>();

            if (queryExecutor == null)
                throw new Exception("query handler is not provided");

            return await queryExecutor.ExecuteAsync(query);
        }
    }

    public interface IQuery
    {

    }

    public interface IQueryResult
    {

    }
}
