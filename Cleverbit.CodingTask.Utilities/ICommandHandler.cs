﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Cleverbit.CodingTask.Utilities
{
    public interface ICommandHandler<T> where T : ICommand
    {
        Task<CommandResult> HandleAsync(T command);
    }

    public class CommandExecutor
    {
        private IServiceProvider _serviceProvider;

        public CommandExecutor(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task<CommandResult> ExecuteAsync<T>(T command) where T : ICommand
        {
            var commandType = typeof(T);

            var commandHandlerType = typeof(ICommandHandler<>).MakeGenericType(commandType);

            var handler = (ICommandHandler<T>)_serviceProvider.GetService(commandHandlerType);

            return await handler.HandleAsync(command);
        }
    }

    public class CommandResult
    {
        public object Data { get; set; }

        public bool Success { get; set; }

        public List<string> Errors { get; set; }

        public static CommandResult Succeded(object data) => new CommandResult { Data = data, Success = true };

        public static CommandResult Succeded() => new CommandResult { Success = true };

        public static CommandResult Failed(string error) => new CommandResult { Errors = new List<string> { error } };
        public static CommandResult Failed(List<string> errors) => new CommandResult { Errors = errors };
    }

    public interface ICommand
    {


    }
}
